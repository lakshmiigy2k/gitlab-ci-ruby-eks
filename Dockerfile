# Base image
FROM ruby:3.0.1-alpine3.13

# Set the working directory
WORKDIR /app

# Copy the Gemfile and Gemfile.lock to the container
COPY Gemfile Gemfile.lock ./

# Install dependencies
RUN apk add --update --no-cache \
    build-base \
    postgresql-dev \
    tzdata \
  && gem install bundler \
  && bundle install

# Copy the rest of the application to the container
COPY . .

# Expose the default HTTP port
EXPOSE 3000

# Start the application
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
